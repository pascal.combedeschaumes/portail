Arbres binaires
===============


Travaux dirigés

* sujet [arbre1.md](arbre1.md)
* éléments de correction dans le notebook arbre (voir ci-dessous)

Travaux pratiques

* sujet [arbre2.md](arbre2.md)
* notebook jupyter complémentaire
  - [arbre.ipynb](arbre.ipynb) 
  - également en ligne via le serveur `jupyter.fil.univ-lille1.fr`
	accessible via 
	[frama.link/diu-ipynb-arbre](https://frama.link/diu-ipynb-arbre)
* module [binary_tree.py](binary_tree.py)


