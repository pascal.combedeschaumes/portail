# choisir l'implémentation du reducer en changeant l'import utilisé
import reducer1 as reducer
#import reducer2  as reducer




'''
    algorithme "générique" qui est construit à l'aide de deux fonctions
        définies par l'interface 'reducer' : prepare() et execute()
    les implémentations de ces fonctions peuvent varier selon le processus
        de "réduction" mis en oeuvre
'''
def apply_reduce(list):
    '''
    @param {list(chaine)}  list : la liste des chaînes à manipuler
    @return {int} la valeur fournie par l'opération de reduction
    '''
    ready_to_transform = reducer.prepare(list)
    result = reducer.execute(ready_to_transform)
    return result




#print(apply_reduce(["timoleon", "abracadabra", "banane", "abc"]) )






if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        module = 'reducer1'
    else:
        module = sys.argv[1]
    reducer = __import__(module)
    
    print( apply_reduce(["timoleon", "abracadabra", "banane", "abc"]) )
