# fonctions de "l'interface" reduce
def prepare(list):
    '''
    @param {list(chaine)}  list : la liste des chaînes à préparer
    @return {list(int)} : liste des longueurs des chaines de list
    >>> prepare(["timoleon", "abracadabra", "banane", "abc"]) ==   [8,11,6,3]
    True
    '''
    return [ len(chaine) for chaine in list]
    
def execute(list):
    '''
    @param {list(int)} list : une liste d'entiers
    @return {int} la plus grande des valeurs de list
    >>> execute([8,11,6,3])  == 11
    True
    '''
    return max(list)



    
if __name__ == '__main__':
    import doctest
    doctest.testmod()